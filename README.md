# parking_lot

# Functional Suite

`bin/test/` contains the Mocha based automated testing suite that will validate the correctness of your program for the sample input and output.

You can use Windows machine to run this code, by installating respective OS from (https://nodejs.org/en/download/package-manager/).If you don't have access to an OSX or Linux machine, you can use a Linux machine where you can develop against using something like [VirtualBox](https://www.virtualbox.org/) or [Docker](https://docs.docker.com/docker-for-windows/#test-your-installation).

This needs [Node.js to be installed](https://nodejs.org/en/download/package-manager/), followed by some libraries. The steps are listed below.

## Setup

First, install [Node.js](https://nodejs.org/en/download/). Then run the following commands under the `parking_lot` dir.

```
parking_lot $ node -v # confirm node present
v10.15.3

```
# Install dependencies, Run test suite & then run project with following command.
parking_lot $ bin/setup.sh  
npm WARN parking_lot@1.0.0 No description
npm WARN parking_lot@1.0.0 No repository field.

audited 195 packages in 3.563s
found 0 vulnerabilities

Creating documents
DB Connected!
    ✓ creates a parking_lot (463ms)

  Reading parking lot details
    ✓ finds parking lot which is empty
    ✓ finds allocated parking lot list

  Upadte parking lot
    ✓ sets and saves parking using an instance
    ✓ update parking using instance
    ✓ update one parking with mentioned slot number
    ✓ empty one parking slot registration number from mentioned slot number (144ms)


  7 passing (3s)

Created parking lot with 6 slots
Allocated parking lot:1
Allocated parking lot:2
Allocated parking lot:3
Allocated parking lot:4
Allocated parking lot:5
Allocated parking lot:6
Registration number KA-01-HH-3141  with Slot Number 6 is free with Charge 30
Slot No.	 Registration No.
1	KA-01-HH-1234
3	KA-01-BB-0001
2	KA-01-HH-9999
5	KA-01-HH-2701
4	KA-01-HH-7777
Allocated parking lot:6
Sorry, parking lot is full
Registration number KA-01-HH-1234  with Slot Number 1 is free with Charge 30
Registration number KA-01-BB-0001  with Slot Number 3 is free with Charge 50
Registration number DL-12-AA-9999 not found
Allocated parking lot:1
Allocated parking lot:3
Sorry, parking lot is full
Slot No.	 Registration No.
1	KA-09-HH-0987
3	CA-09-IO-1111
2	KA-01-HH-9999
5	KA-01-HH-2701
6	KA-01-P-333
4	KA-01-HH-7777

```
```
# Another option
parking_lot $ npm install --save  --prefix ./bin/ ./bin/  # install dependencies
npm WARN parking_lot@1.0.0 No description
npm WARN parking_lot@1.0.0 No repository field.

audited 195 packages in 3.563s
found 0 vulnerabilities

parking_lot $ npm test --prefix ./bin/ # run test suite
  Creating documents
DB Connected!
    ✓ creates a parking_lot (463ms)

  Reading parking lot details
    ✓ finds parking lot which is empty
    ✓ finds allocated parking lot list

  Upadte parking lot
    ✓ sets and saves parking using an instance
    ✓ update parking using instance
    ✓ update one parking with mentioned slot number
    ✓ empty one parking slot registration number from mentioned slot number (144ms)


  7 passing (3s)

parking_lot $ node bin/parking_lot # run the project
Created parking lot with 6 slots
Allocated parking lot:1
Allocated parking lot:2
Allocated parking lot:3
Allocated parking lot:4
Allocated parking lot:5
Allocated parking lot:6
Registration number KA-01-HH-3141  with Slot Number 6 is free with Charge 30
Slot No.	 Registration No.
1	KA-01-HH-1234
3	KA-01-BB-0001
2	KA-01-HH-9999
5	KA-01-HH-2701
4	KA-01-HH-7777
Allocated parking lot:6
Sorry, parking lot is full
Registration number KA-01-HH-1234  with Slot Number 1 is free with Charge 30
Registration number KA-01-BB-0001  with Slot Number 3 is free with Charge 50
Registration number DL-12-AA-9999 not found
Allocated parking lot:1
Allocated parking lot:3
Sorry, parking lot is full
Slot No.	 Registration No.
1	KA-09-HH-0987
3	CA-09-IO-1111
2	KA-01-HH-9999
5	KA-01-HH-2701
6	KA-01-P-333
4	KA-01-HH-7777

```
```
## Usage

You can run the full suite from `parking_lot` by doing
```
parking_lot $ node bin/parking_lot
```
You can run the Mocha based automated testing suite from `parking_lot` by doing
```
parking_lot $ npm test
```


