const parkingSlot = require('../config/mongo');

exports.parkingStatus = function() {
    return new Promise((resolve, reject) => {
        try {
            parkingSlot.countDocuments({'registration_number': ''})
            .then((count)=>{
                parkingSlot.find({'registration_number': {$ne: ''}})
                .then(parkedData => {
                    var parkingStatusText = 'Slot No.\t Registration No.';
                    if(parkedData.length > 0){
                        for(var i in parkedData){
                            parkingStatusText += '\n' + parkedData[i].slot_number + '\t' + parkedData[i].registration_number;
                        }
                    }
                    resolve(parkingStatusText);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        } catch (err) {
            reject(err);
        }
    });
};