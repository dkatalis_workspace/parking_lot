const parkingSlot = require('../config/mongo');

exports.assignParkingSlot = function(registrationNumber) {
    return new Promise((resolve, reject) => {
        try {
            getEmptyParkingCount().then((count) => {
                if(count === 0) {
                    parkingSlot.countDocuments({}).then(docCount=> {
                        docCount === 0 ? resolve('Sorry, parking not available') :resolve('Sorry, parking lot is full');
                    }).catch(err => reject(err));
                } else {
                    parkingSlot.find({'registration_number': registrationNumber}).then((regdocs)=>{
                        if(regdocs.length === 0) {
                            parkingSlot.find({'registration_number': ''}, {'slot_number': true})
                            .sort({'slot_number': 1})
                            .limit(1)
                            .then((docs)=>{
                                parkingSlot.updateOne({'slot_number': docs[0].slot_number}, {'registration_number': registrationNumber}).then(()=> {
                                    parkingSlot.find({'registration_number': registrationNumber})
                                    .then((updatedoc)=>{
                                        resolve(`Allocated parking lot:${updatedoc[0].slot_number}`);
                                    }).catch(err => reject(err));
                                })
                            }).catch(err => reject(err));
                        } else {
                            resolve(`Registration number ${registrationNumber} already allocated Slot Number ${regdocs[0].slot_number}`);
                        }
                    }).catch(err => reject(err));
                }
            }).catch(err => reject(err));
        } catch (err) {
            reject(err);
        }
    });
};

function getEmptyParkingCount() {
    return new Promise((resolve, reject) => {
        try {
            parkingSlot.countDocuments({'registration_number': ''})
            .then(count=> resolve(count))
            .catch(err => reject(err));
        } catch (err) {
            reject(err);
        }
    });
};
