const parkingSlot = require('../config/mongo');

exports.emptyParkingSlot = function(registrationNumber, durationInHours) {
    return new Promise((resolve, reject) => {
        try {
            parkingSlot.find({'registration_number': registrationNumber}).then((regdocs)=>{
                if(regdocs.length !== 0) {
                    parkingSlot.updateOne({'slot_number': regdocs[0].slot_number}, {'registration_number': ''}).then(()=> {
                        var amount = durationInHours > 2 ? (durationInHours - 2) * 10 + 10 : 10;
                        resolve(`Registration number ${registrationNumber}  with Slot Number ${regdocs[0].slot_number} is free with Charge ${amount}`);
                    });
                } else {
                    resolve(`Registration number ${registrationNumber} not found`);
                }
            }).catch(err => reject(err));
        } catch (err) {
            console.log('err', err);
            reject();
        }
    });
};