const parkingSlot = require('../config/mongo');

exports.createParkingLot = function(slotCount) {
    return new Promise((resolve, reject) => {
        try {
            var slotsArray = [];
            for(var i=1; i <= slotCount; i++) {
                slotsArray.push({ 
                    slot_number: i,
                });
            }
            parkingSlot.deleteMany({}).then(() => {
                parkingSlot.create(slotsArray)
                .then(docs => resolve(`Created parking lot with ${docs.length} slots`))
                .catch(err => reject(err));
            }).catch(err => reject(err));
        } catch (err) {
            reject(err);
        }
    });
};

