# Add script to:
# * Install dependencies
# * Build/Compile
# * Run Test Suit to validate
#
# After this is run, bin/parking_lot
# should Just Work.

npm install --save --prefix ./bin/ ./bin/
# echo 'Install dependencies Completed'
npm test  --prefix ./bin/ 
# echo 'Run Test Suit Completed'
node bin/parking_lot