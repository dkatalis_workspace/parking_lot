const assert = require('assert');
const parkingSlot = require('../config/mongo'); 


describe('Creating documents', () => {
    it('creates a parking_lot', (done) => {
        const parking = new parkingSlot({ slot_number: 1 });
        parking.save() 
            .then(() => {
                assert(!parking.isNew); 
                done();
            });
    });
});
