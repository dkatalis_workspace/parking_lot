const assert = require('assert');
const parkingSlot = require('../config/mongo'); 

describe('Upadte parking lot', () => {

    let parking;

    beforeEach((done) => {
        parking = new parkingSlot({ slot_number: 1});
        parking.save() 
            .then(() => {
                done();
            });
    });
  
  function assertHelper(statement, done) {
    statement
     .then(() => parkingSlot.find({}))
     .then((parking) => {
      assert(parking.length === 1);
      assert(parking[0].slot_number === 1);
      done();
    });
  }
  
  it('sets and saves parking using an instance', (done) => {
    parking.set('slot_number', 1); 
    assertHelper(parking.save(), done);
   });
 
  it('update parking using instance', (done) => {
    assertHelper(parkingSlot.updateMany({ registration_number: 'KA-01-HH-1234' }), done);
  });

  it('update one parking with mentioned slot number', (done) => {
    assertHelper(parkingSlot.updateOne({ slot_number: 1 }, { registration_number: 'KA-01-HH-1234' }), done);
  });

  it('empty one parking slot registration number from mentioned slot number', (done) => {
    assertHelper(parkingSlot.updateOne({ slot_number: 1 }, { registration_number: '' }), done);
  });
});