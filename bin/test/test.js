var assert = require('assert');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/parking_lot', {useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('DB Connected!'))
  .catch(err => {
    console.log("err", err);
  });

beforeEach((done) => {
  mongoose.connection.collections.parking_slots.drop(() => {
       //this function runs after the drop is completed
      done(); //go ahead everything is done now.
  }); 
});

