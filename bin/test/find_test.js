const assert = require('assert');
const parkingSlot = require('../config/mongo'); 
let parking;

describe('Reading parking lot details', () => {
    beforeEach((done) => {
        parking = new parkingSlot({ slot_number: 1});
        parking.save() 
            .then(() => {
                done();
            });
    });
    
    it('finds parking lot which is empty', (done) => {
        parkingSlot.findOne({ registration_number: '' })
            .then((parking) => {
                assert(parking.length !== 0); 
                done();
            });
    });

    it('finds allocated parking lot list', (done) => {
        parkingSlot.updateOne({ slot_number: 1 }, { registration_number: 'KA-01-HH-1234' }).then(() => {
            parkingSlot.find({ registration_number: {$ne: '' }})
            .then((parking) => {
                assert(parking.length !== 0); 
                done();
            });
        });
    });
});