const fs = require('fs');
const createParking = require('./actions/createParking');
const assignParking = require('./actions/assignParking');
const emptyParking = require('./actions/emptyParking');
const parkingStatus = require('./actions/parkingStatus');


fs.readFile('./bin/parking_lot file_inputs.txt', 'utf8', (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  executeInstructions(data.trim().split(/\n/g));
});

async function executeInstructions(instructionArry){
  for(var i  in instructionArry){
    // console.log(instructionArry[i].split(/\s/g));
    var instruction = instructionArry[i].split(/\s/g); 
    if(instruction[0] === 'create_parking_lot') {
      await createParking.createParkingLot(instruction[1]).then(res => console.log(res)).catch(err => console.log(err));
    } else if(instruction[0] === 'park') {
      await assignParking.assignParkingSlot(instruction[1]).then(res => console.log(res)).catch(err => console.log(err));
    } else if(instruction[0] === 'leave') {
      await emptyParking.emptyParkingSlot(instruction[1], instruction[2]).then(res => console.log(res)).catch(err => console.log(err));;
    } else if(instruction[0] === 'status') {
      await parkingStatus.parkingStatus().then(res => console.log(res)).catch(err => console.log(err));
    } 
  }
}