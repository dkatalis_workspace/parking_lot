var mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/parking_lot', {useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {})
  .catch(err => {
    console.log("err", err);
  });
  
var parkingSlotSchema = new Schema({
    slot_number: {
        type: Number,
        required: [true, 'Slot Number is required.']
    },
    registration_number: {
        type: String,
        required: [false],
        default: ''
    }
});

const parkingSlot = mongoose.model('parking_slot', parkingSlotSchema);
module.exports = parkingSlot;
